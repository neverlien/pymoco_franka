# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2018"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import threading
import socket
import struct
import time

import numpy as np

from .. import robot
from . import RobotFacade
from .. import kinematics

# Packet state and type constants
NONE=0
CLIENT=1
SERVER=2
REJECTED=3
QUIT=4
INIT=5
ACK=6
CONN=7

class _Packet:
    def __init__(self, *args):
        if len(args) == 0:
            self.data = 17*[0]
        elif len(args) == 1 and type(args[0]) == bytes:
            self.bytes = data
        elif len(args) == 17:
            data = args
        else:
            raise Exception("_Packet.__init__: Need 0, 1 or 17 arguments for initialization.")
    def __repr__(self):
        return 'cid:{o.cycle_id} state:{o.state} type:{o.type}'.format(o=self)

    def get_data(self):
        return ([self.cycle_id, self.state, self.type] +
                list(self.joint_positions_act) +
                list(self.joint_positions_cmd))

    def set_data(self, data):
        self.cycle_id = data[0]
        self.state = data[1]
        self.type = data[2]
        self.joint_positions_act = list(data[3:3+7])
        self.joint_positions_cmd = list(data[3+7:])

    data = property(get_data, set_data)
        
    def get_bytes(self):
        return packet_struct.pack(*self.data)

    def set_bytes(self, bytes):
        data = packet_struct.unpack(bytes)
        self.data = data

    bytes = property(get_bytes, set_bytes)


# _Packet struct
packet_struct = struct.Struct('<BBB7d7d') # Cycle ID, State, Type, Joint Pos Act, Joint Pos Cmd


class FMSFacade(RobotFacade, threading.Thread):
    
    def __init__(self, **kwargs):
        self._log_level = kwargs.get('log_level', 2)
        RobotFacade.__init__(self, **kwargs)
        self._rob_def = robot.create_robot('Emika')
        self._frame_computer = kinematics.FrameComputer(
            rob_def=self._rob_def)
        self._fms_host = kwargs.get('fms_host')
        self._fms_port = kwargs.get('fms_port')
        self._fms_addr = (self._fms_host, self._fms_port)
        threading.Thread.__init__(self, name=repr(self))
        self.daemon = True
        self.__stop = False
        self._fms_sock = socket.socket(type=socket.SOCK_DGRAM)
        self._packet_counter = 0
        self._cmdp = _Packet()
        self._cmdp.type = CLIENT
        self._t_packet = None
        self._t_pll = None
        self._cycle_time = kwargs.get('cycle_time', 0.01)
        
    def __repr__(self):
        return '{}<{}:{}>'.format(self.__class__.__name__, self._fms_host, self._fms_port)
    
    def get_control_period(self):
        return self._cycle_time
    control_period = property(get_control_period)

    def get_current_arrival_time(self):
        """Return the time at which the current, i.e. latest, status
        packet was received from the robot controller."""
        return self._t_pll # self._t_packet

    current_arrival_time = property(get_current_arrival_time)

    def get_act_joint_pos(self):
        return self._q_act

    def get_cmd_joint_pos(self):
        return self._q_tracked

    def set_cmd_joint_pos(self, cmd_jpos):
        self._q_tracked = cmd_jpos
        self._cmdp.joint_positions_cmd = self._q_tracked
        self._fms_sock.sendto(self._cmdp.bytes, self._fms_addr)
        with self._control_cond:
            self._control_cond.notify_all()

    cmd_joint_pos = property(get_cmd_joint_pos, set_cmd_joint_pos)

    def set_cmd_joint_increment(self, cmd_jinc):
        self.set_cmd_joint_pos(self._q_tracked + cmd_jinc)

    cmd_joint_increment = property(None, set_cmd_joint_increment)
        
    def stop(self):
        # Send a quit packet
        quitp = _Packet()
        quitp.type = QUIT
        self._fms_sock.sendto(quitp, self._fms_addr)
        self.__stop = True
        self.join()

    def run(self):
        # Set up for interaction
        initp = _Packet()
        initp.type = INIT
        actp = _Packet()
        ackp = _Packet()
        # Initialize
        self._fms_sock.sendto(initp.bytes, self._fms_addr)
        # Get ack
        ackp.bytes = self._fms_sock.recv(4096)
        if ackp.type == REJECTED:
            raise Exception('Franka facade got rejected')
        else:
            print('Accepted')
            self._q_act = self._q_tracked = ackp.joint_positions_act
            self._t_packet = self._t_pll = time.time()
            self._cycle_id = ackp.cycle_id
            # self._event_publisher.publish(time.time())
        while not self.__stop:
            actp.bytes = self._fms_sock.recv(4096)
            self._t_packet_last = self._t_packet
            self._t_pll_last = self._t_packet
            self._t_packet = time.time()
            self._t_pll += self._cycle_time  + 0.01 * (self._t_packet_last - self._t_pll_last)
            self._q_act = actp.joint_positions_act         
            self._event_publisher.publish(self._t_packet)
            #self._cycle_time = 0.1 * (self._t_packet - self._t_packet_last) + 0.9 * self._cycle_time
